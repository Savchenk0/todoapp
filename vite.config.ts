import { defineConfig } from "vite"
import react from "@vitejs/plugin-react"
import path from "node:path"
// https://vitejs.dev/config/
export default defineConfig({
	plugins: [react()],
	resolve: {
		alias: {
			"@components": path.resolve(__dirname, "./src/components"),
			"@hooks": path.resolve(__dirname, "./src/hooks"),
			"@styledComponents": path.resolve(__dirname, "./src/styledComponents"),
			"@store": path.resolve(__dirname, "./src/store"),
		},
	},
})
