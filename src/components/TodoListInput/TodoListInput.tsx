import {
	AddButton,
	ErrorMessage,
	InputWrapper,
	ListInput,
	ListInputWrapper,
} from "@styledComponents/MuiDerrivedComponents"
import { useAppSelector } from "@hooks/useRedux"
type TodoListInputProps = {
	addTodo: () => void
	inputValue: string
	setInputValue: React.Dispatch<React.SetStateAction<string>>
}

export default function TodoListInput({
	addTodo,
	inputValue,
	setInputValue,
}: TodoListInputProps) {
	const error = useAppSelector(state => state.error.error)
	const handleKeyDown: React.KeyboardEventHandler<HTMLDivElement> = event => {
		if (event.key === "Enter") {
			addTodo()
		}
	}
	return (
		<InputWrapper>
			<ListInputWrapper>
				<ListInput
					id="standard-basic"
					placeholder="Enter your task here..."
					value={inputValue}
					onKeyDown={handleKeyDown}
					variant="standard"
					onChange={event => {
						setInputValue(event.target.value)
					}}
				/>
				{error && <ErrorMessage>{error}</ErrorMessage>}
			</ListInputWrapper>
			<AddButton onClick={addTodo} variant="outlined" disableRipple>
				Add to-do
			</AddButton>
		</InputWrapper>
	)
}
