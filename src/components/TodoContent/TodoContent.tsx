import { useState } from "react"
import {
	TodoHeader,
	TodoTitle,
	TodoContentWrapper,
	FilterWrapper,
	ContentBox,
	ContentWrapper,
	DeleteAllButton,
} from "@styledComponents/MuiDerrivedComponents.js"
import nextId from "react-id-generator"
import { FilterButton } from "@components/FilterButton/FilterButton"
import TodoList from "@components/TodoList/TodoList"
import useCurrentTodos from "@hooks/useTodos"
import useLocalStorage from "@hooks/useLocalStorage"
import TodoListInput from "@components/TodoListInput/TodoListInput"
import {
	addBlankErrorAC,
	addLongErrorAC,
	removeErrorAC,
} from "@store/actionCreators"
import { useAppDispatch } from "@hooks/useRedux"
import Counter from "@components/Counter/Counter"
export interface todoI {
	text: string
	isDone: boolean
	id: string
}
export const filterOptions = ["all", "todo", "done"]
export default function TodoContent() {
	const prevTodos = localStorage.getItem("todosArray")
	const [todos, setTodos] = useState<todoI[]>(
		prevTodos ? JSON.parse(prevTodos) : []
	)
	const dispatch = useAppDispatch()
	const [inputValue, setInputValue] = useState("")
	const [currentFilterIndex, setCurrentFilterIndex] = useState(0)

	const { filteredTodos, amountData } = useCurrentTodos(
		todos,
		filterOptions,
		currentFilterIndex
	)
	const validateTodo = () => {
		if (inputValue.length > 22) {
			dispatch(addLongErrorAC())
			return false
		}

		if (!inputValue.replaceAll(" ", "")) {
			dispatch(addBlankErrorAC())
			return false
		}
		return true
	}
	const addTodo = () => {
		if (!validateTodo()) return
		setTodos(prev => [
			...prev,
			{ text: inputValue, isDone: false, id: nextId() },
		])
		setInputValue("")
		dispatch(removeErrorAC())
	}
	const handleDeleteAll = () => {
		if (currentFilterIndex === 0) {
			setTodos([])
			return
		}
		setTodos(prev => prev.filter(todo => !filteredTodos.includes(todo)))
	}
	useLocalStorage(todos, "todosArray")
	return (
		<ContentWrapper>
			<ContentBox>
				<TodoHeader>
					<TodoTitle>My todo list</TodoTitle>
				</TodoHeader>
				<Counter {...amountData} />
				<FilterWrapper>
					{filterOptions.map((option, index) => (
						<FilterButton
							key={nextId()}
							isActive={currentFilterIndex === index}
							handleClick={() => setCurrentFilterIndex(index)}
						>
							{option}
						</FilterButton>
					))}
				</FilterWrapper>
				<TodoContentWrapper className="content">
					{currentFilterIndex !== 2 ? (
						<TodoListInput
							inputValue={inputValue}
							setInputValue={setInputValue}
							addTodo={addTodo}
						/>
					) : null}

					<TodoList setTodos={setTodos} todos={filteredTodos} />
					{filteredTodos.length ? (
						<DeleteAllButton
							onClick={handleDeleteAll}
							variant="outlined"
							color="warning"
						>
							Delete all
						</DeleteAllButton>
					) : null}
				</TodoContentWrapper>
			</ContentBox>
		</ContentWrapper>
	)
}
