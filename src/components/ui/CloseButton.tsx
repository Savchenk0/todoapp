import { IconButton } from "@mui/material"
import ClearIcon from "@mui/icons-material/Clear"

type CloseButtonProps = {
	handleDelete: (id: string) => void
	id: string
}
export default function CloseButton({ handleDelete, id }: CloseButtonProps) {
	return (
		<IconButton
			onClick={() => {
				handleDelete(id)
			}}
		>
			<ClearIcon />
		</IconButton>
	)
}
