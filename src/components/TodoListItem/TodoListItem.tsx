import { Checkbox, ListItemText } from "@mui/material"
import {
	StyledListItem,
	StyledListItemButton,
} from "@styledComponents/MuiDerrivedComponents"
import CloseButton from "@components/ui/CloseButton"
import { todoI } from "@components/TodoContent/TodoContent"
type TodoListItemProps = {
	todo: todoI
	handleIsDoneChange: (id: string) => void
	handleDelete: (id: string) => void
}
export default function TodoListItem({
	todo,
	handleIsDoneChange,
	handleDelete,
}: TodoListItemProps) {
	const { isDone, id, text } = todo
	return (
		<StyledListItem>
			<StyledListItemButton
				onClick={() => handleIsDoneChange(id)}
				disableRipple
			>
				<Checkbox edge="start" checked={isDone} />
				{isDone ? (
					<ListItemText
						primaryTypographyProps={{
							fontFamily: "'Montserrat', sans-serif",
							fontSize: "16px",
							color: "#808080",
						}}
					>
						<del>{text}</del>
					</ListItemText>
				) : (
					<ListItemText
						primaryTypographyProps={{
							fontFamily: "'Montserrat', sans-serif",
							fontSize: "16px",
						}}
					>
						{text}
					</ListItemText>
				)}
			</StyledListItemButton>
			<CloseButton
				id={id}
				handleDelete={() => {
					handleDelete(id)
				}}
			/>
		</StyledListItem>
	)
}
