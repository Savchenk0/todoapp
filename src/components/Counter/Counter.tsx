import { Typography } from "@mui/material"
import {
	CounterWrapper,
	CounterContent,
} from "@styledComponents/MuiDerrivedComponents"
interface CounterI {
	todo: number
	done: number
}
const Counter = ({ todo, done }: CounterI) => {
	return (
		<CounterWrapper>
			<CounterContent>
				<Typography sx={{ fontSize: "16px" }}>You have done:{done}</Typography>
				<Typography sx={{ fontSize: "16px" }}>Left to do:{todo}</Typography>
			</CounterContent>
		</CounterWrapper>
	)
}
export default Counter
