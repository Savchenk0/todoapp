import { todoI } from "@components/TodoContent/TodoContent"
import TodoListItem from "@components/TodoListItem/TodoListItem"
import {
	BlankListText,
	StyledList,
} from "@styledComponents/MuiDerrivedComponents"

type TodoListProps = {
	todos: todoI[]
	setTodos: React.Dispatch<React.SetStateAction<todoI[]>>
}
export default function TodoList({ todos, setTodos }: TodoListProps) {
	const handleIsDoneChange = function (id: string) {
		setTodos(prevTodos => {
			const newTodos = [...prevTodos].map(todo => {
				if (todo.id !== id) return todo
				return { ...todo, isDone: !todo.isDone }
			})
			return newTodos
		})
	}
	const handleDelete = function (id: string) {
		setTodos(prevTodos => {
			const newTodos = [...prevTodos].filter(todo => todo.id !== id)
			return newTodos
		})
	}
	return (
		<StyledList>
			{todos.length ? (
				todos.map(todo => {
					return (
						<TodoListItem
							key={todo.id}
							handleIsDoneChange={handleIsDoneChange}
							handleDelete={handleDelete}
							todo={todo}
						/>
					)
				})
			) : (
				<BlankListText>You currently have no tasks here</BlankListText>
			)}
		</StyledList>
	)
}
