import { FilterBtn } from "@styledComponents/MuiDerrivedComponents"
type FilterButtonProps = {
	isActive: boolean
	handleClick: () => void
	children: React.ReactNode
}
export const FilterButton = ({
	isActive,
	handleClick,
	children,
	...props
}: FilterButtonProps) => {
	return (
		<FilterBtn
			className={isActive ? "_active" : ""}
			onClick={handleClick}
			{...props}
		>
			{children}
		</FilterBtn>
	)
}
