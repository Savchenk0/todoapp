import { useEffect } from "react"

const useLocalStorage: <Type>(variable: Type, itemName: string) => void = (
	variable,
	itemName
) => {
	useEffect(() => {
		localStorage.setItem(itemName, JSON.stringify(variable))
	}, [variable, itemName])
}

export default useLocalStorage
