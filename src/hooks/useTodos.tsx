import { useMemo } from "react"
import { todoI } from "@components/TodoContent/TodoContent"

const useTodos = (
	todos: todoI[],
	filterOptions: string[],
	currentFilterIndex: number
) => {
	const filteredTodos = useMemo(() => {
		if (!currentFilterIndex) return [...todos]
		if (filterOptions[currentFilterIndex] === "done") {
			return todos.filter(todo => todo.isDone)
		}
		return todos.filter(todo => !todo.isDone)
	}, [filterOptions, currentFilterIndex, todos])
	const amountData = todos.reduce(
		(acc, el) =>
			el.isDone
				? { ...acc, done: acc.done + 1 }
				: { ...acc, todo: acc.todo + 1 },
		{ todo: 0, done: 0 }
	)
	return { filteredTodos, amountData }
}
export default useTodos
