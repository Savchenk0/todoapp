import ReactDOM from "react-dom/client"
import App from "./App"
import "./index.css"
import { Provider } from "react-redux"
import store from "@store/index"
const rootDiv = document.getElementById("root") as HTMLElement

ReactDOM.createRoot(rootDiv).render(
	<Provider store={store}>
		<App />
	</Provider>
)
