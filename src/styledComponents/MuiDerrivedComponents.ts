import {
	TextField,
	Box,
	Typography,
	ListItem,
	List,
	ListItemButton,
	Button,
} from "@mui/material"
import { styled } from "@mui/system"

export const ListInput = styled(TextField)({
	width: "100%",
	" .MuiInput-input": {
		fontSize: "16px",
		paddingLeft: "20px",
	},
})
export const InputWrapper = styled(Box)({
	marginTop: "15px",
	display: "flex",
	alignItems: "flex-start",
	columnGap: "30px",
})

export const ListInputWrapper = styled(Box)({
	flexGrow: 1,
	textAlign: "left",
})
export const TodoHeader = styled(Box)({
	display: "flex",
	justifyContent: "center",
	alignItems: "center",
	padding: "15px 15px 15px 0",
})
export const TodoTitle = styled(Typography)({
	padding: 0,
	margin: 0,
	fontSize: "28px",
	fontFamily: "sans-serif",
	fontWeight: "bold",
})
export const TodoContentWrapper = styled(Box)({
	textAlign: "right",
})
export const ContentWrapper = styled(Box)({
	display: "flex",
	alignItems: "center",
	flexDirection: "column",
})
export const StyledListItem = styled(ListItem)({
	margin: "2px 0 ",
	padding: "5px",
	paddingLeft: "15px",
	cursor: "default",
	display: "flex",
	justifyContent: "space-between",
	borderRadius: "4px",
})

export const StyledList = styled(List)({
	display: "flex",
	flexDirection: "column",
})
export const StyledListItemButton = styled(ListItemButton)({
	padding: 0,
})
export const FilterBtn = styled(Button)({
	borderRadius: 0,
	flexBasis: "150px",
	flexShrink: 0,
	":hover": {
		// backgroundColor: "#FFFFFF",
	},
	"&._active": {
		borderBottom: "1px solid black",
	},
})

export const BlankListText = styled(Typography)({
	fontSize: "28px",
	textAlign: "center",
	marginTop: "30px",
})
export const FilterWrapper = styled(Box)({
	display: "flex",
	justifyContent: "space-between",
	borderBottom: "1px solid black",
})
export const ContentBox = styled(Box)({
	width: "500px",
})
export const AddButton = styled(Button)({
	borderRadius: "10px",
})
export const DeleteAllButton = styled(Button)({
	marginTop: "20px",
})
export const ErrorMessage = styled(Typography)({
	color: "#ff0000",
	marginLeft: "10px",
	marginTop: "5px",
})
export const CounterWrapper = styled(Box)({
	display: "flex",
	justifyContent: "center",
	marginBottom: "35px",
})
export const CounterContent = styled(Box)({
	display: "flex",
	borderRadius: "5px",
	columnGap: "35px",
	width: "fit-content",
	padding: "10px",
})
