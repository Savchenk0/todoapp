import { ADD_ERROR, REMOVE_ERROR } from "@store/actions"

type initialStateT = {
	error: string
}
const initialState: initialStateT = {
	error: "",
}
const errorReducer = (
	state = initialState,
	action: { type: string; payload?: string }
) => {
	switch (action.type) {
		case ADD_ERROR: {
			return { ...state, error: action.payload }
		}
		case REMOVE_ERROR: {
			return { ...state, error: "" }
		}
		default: {
			return state
		}
	}
}
export default errorReducer
