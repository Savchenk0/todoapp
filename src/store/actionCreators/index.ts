import { ADD_ERROR, REMOVE_ERROR } from "@store/actions"

export const addBlankErrorAC = () => ({
	type: ADD_ERROR,
	payload: "Todo can't be blank",
})
export const addLongErrorAC = () => ({
	type: ADD_ERROR,
	payload: "Todo should be less than 23 characters long",
})
export const removeErrorAC = () => ({
	type: REMOVE_ERROR,
	payload: "",
})
